﻿using UnityEngine;
using UnityEditor;
using System.Text;
using System.IO;
using System.Collections.Generic;
using System.Xml.Serialization;

public class meshSimpTool : EditorWindow
{
    public static string URL = "Assets\\meshDB\\";
    List<loadedMesh> meshes = new List<loadedMesh>();
    loadedMesh[] tmp_meshes = new loadedMesh[0];
    loadedMesh activeMesh;

    loadMesh lm;

    Vector2 previewDimensions = new Vector2(256, 256);

    private GameObject cam = null;
    private Camera Camera
    {
        get
        {
            if (cam == null)
            {
                cam = new GameObject();
                cam.hideFlags = HideFlags.HideAndDontSave;
                cam.transform.position = new Vector3(0.5f, 2.0f, -5.0f);
                cam.transform.eulerAngles = new Vector3(19.0f, -5.0f, 0.0f);
                cam.AddComponent<Camera>();
                cam.GetComponent<Camera>().fieldOfView = 50;
                
            }
            return cam.GetComponent<Camera>();
        }
    }


    [MenuItem("Tools/toolik")]
    private static void showEditor(){
        EditorWindow.GetWindow<meshSimpTool>(false, "Progresivne Meshe");
    }

    void OnGUI() {
        getMeshList();
        /*
        *   Load Mesh Button, loads a obj file and save it in 100 file with decreasing complexity
        */
        if (GUILayout.Button("Load Mesh")){
            string meshFilePath = EditorUtility.OpenFilePanel("Load Mesh", "", "obj");
            lm = new loadMesh(meshFilePath);
        }

        /*
        *   Mesh List view, with active button and delete button
        */
        GUILayout.Label("Mesh List", EditorStyles.boldLabel);
        for(int i = 0; i < tmp_meshes.Length; i++){
            GUILayout.BeginHorizontal();

            EditorGUILayout.LabelField((tmp_meshes[i].name.Length > 12)? tmp_meshes[i].name.Substring(0,9)+"...": tmp_meshes[i].name/*, (tmp_meshes[i].active)?EditorStyles.boldLabel: GUILayout.MaxHeight(50f)*/);

            //Active button, set item active and load mesh
            if (GUILayout.Button("Set Active")){
                if (activeMesh == null || activeMesh.active == false){
                    tmp_meshes[i].setActive(true);
                    activeMesh = tmp_meshes[i];

                } else {
                    activeMesh.setActive(false);
                    tmp_meshes[i].setActive(true);
                    activeMesh = tmp_meshes[i];

                }
                Debug.Log(activeMesh.name);
            }
            //Delete button TODO
            if (GUILayout.Button("X", GUILayout.Width(20f))) {
                tmp_meshes[i] = null;
            }

            GUILayout.EndHorizontal();
        }

        /*
        * Mesh Preview Section
        */
        GUILayout.Label("Mesh Preview", EditorStyles.boldLabel);

        if (activeMesh != null && activeMesh.active){
            GUILayout.BeginHorizontal();

                activeMesh.complexity = EditorGUILayout.IntSlider(activeMesh.complexity, 1, 100);
                if (GUILayout.Button("Download")){
                    /* string path = EditorUtility.SaveFilePanel(
                     "Save asset",
                     "",
                     activeMesh.name + ".asset",
                     "asset");*/
                    string path = "Assets//" + activeMesh.name + activeMesh.complexity + ".asset";
                    activeMesh.saveMesh(path);
                }

            GUILayout.EndHorizontal();

            if (GUI.changed)
                activeMesh.changeComplexity(activeMesh.complexity);

            /*
            *   Camera Preview
            */
            Graphics.DrawMesh(activeMesh.m, Vector3.zero, Quaternion.identity, new Material(Shader.Find("Standard")), 0, Camera);
            GUI.BeginGroup(GUILayoutUtility.GetRect(previewDimensions.x, previewDimensions.y));
            Handles.BeginGUI();
            Handles.DrawCamera(new Rect(0, 0, this.position.width, 256), Camera, DrawCameraMode.TexturedWire);
            Handles.EndGUI();
            GUI.EndGroup();
        }
        

    }

    void getMeshList() {
        if (!File.Exists(URL + "list.txt"))
            return;

        string[] meshList;
        StreamReader sr = new StreamReader(URL + "list.txt", Encoding.Default);                   
            meshList = sr.ReadToEnd().Split(';');
        sr.Close();

        tmp_meshes = new loadedMesh[meshList.Length - 1];

        for (int i = 0; i < meshList.Length - 1; i++){
            tmp_meshes[i] = new loadedMesh(URL + meshList[i] + "\\", meshList[i]);                       
        }

    }

    void deleteMeshFromList(int index) {

    }

    class loadedMesh {
        public Mesh m;
        public string name;
        public int complexity;
        public bool active;

        string path;

        public loadedMesh(string path, string name, int complexity = 100, bool active = false) {
            if(path != "") {
                this.path = path;
            }
            this.name = name;
            this.complexity = complexity;
            this.active = active; 
        }
        public void changeComplexity(int c) {
            m = null;
            buildMesh();
        }

        public void setActive(bool active) {
            if (active) {
                buildMesh();            
            }else {
                this.m = null;
            }
            this.active = active;
        }
        private void buildMesh() {
            Vector3[] verticies = new Vector3[0];
            int[] triangles = new int[0];
            parseInputFile(out verticies, out triangles);

            m = new Mesh();
            m.vertices = verticies;
            m.triangles = triangles;

            m.RecalculateNormals();
            m.RecalculateBounds();
            m.Optimize();

        }

        private void parseInputFile(out Vector3[] ver, out int[] tria) {
            string tmp = path + complexity + ".bin";
            string[] param = File.ReadAllText(path + complexity + ".bin").Split(';');
            string[] verticiesRaw = param[0].Split(':');

            ver = new Vector3[verticiesRaw.Length];

            for (int i = 0; i < verticiesRaw.Length; i++)
            {
                string[] vals = verticiesRaw[i].Split(',');
                ver[i] = new Vector3(loadMesh.cf(vals[0]), loadMesh.cf(vals[1]), loadMesh.cf(vals[2]));
            }
            string[] trianRaw = param[1].Split(',');
            tria = new int[trianRaw.Length];
            for (int i = 0; i < trianRaw.Length; i++)
            {
                tria[i] = loadMesh.ci(trianRaw[i]);
            }
        }

        public void saveMesh(string path) {
            if (path == "")
                return;
            /*MeshRenderer mr = new MeshRenderer();
            mr.material = new Material(Shader.Find("Standard"));
            var mat = new Material(Shader.Find("Standard"));*/
            
            AssetDatabase.CreateAsset(this.m, path);
           // AssetDatabase.AddObjectToAsset(mr, mat);
            AssetDatabase.SaveAssets();
            AssetDatabase.Refresh();
        }

    }



    
}


class loadMesh
{
    string URL =  @"Assets\\meshDB\\";
    string material, uml, name;
    List<Vector3> verticies = new List<Vector3>();
    List<Vector2> uvs = new List<Vector2>();
    List<Vector3> normals = new List<Vector3>();
    List<Vector3> faces = new List<Vector3>();
    List<int> triangles = new List<int>();

    progMesh pm;

    public loadMesh(string path)
    {
        if (path == "" || path == null)
        {
            Debug.Log("Mesh couldnt be loaded");
            return;
        }

        StreamReader sr = new StreamReader(path, Encoding.Default);
        processMesh(sr);
        sr.Close();
        
        string savePath = URL + name;
        Directory.CreateDirectory(savePath);

        using (StreamWriter w = File.AppendText(URL + "list.txt")) {
            w.Write(name + ";");
            w.Close();
        }

        using (StreamWriter w = File.AppendText(savePath + "\\100.bin"))
        {
            for (int i = 0; i < verticies.Count; i++) {
                w.Write(verticies[i].x + "," + verticies[i].y + "," + verticies[i].z);
                if (i < verticies.Count - 1)
                    w.Write(':');
            }
            w.Write(";");
            for (int i = 0; i < triangles.Count; i++)
            {
                w.Write(triangles[i]);
                if (i < triangles.Count - 1)
                    w.Write(',');
            }
            w.Close();
        }       

       pm = new progMesh(verticies, triangles, this.name);

    }
    
    private void processMesh(StreamReader input)
    {
        /*if (input == null)
        {
            Debug.Log("Mesh couldnt be loaded");
            return;
        }*/
        string line;
        while (!input.EndOfStream)
        {

            line = input.ReadLine();

            if (line == "" || line[0] == '#' ) continue;

            if (line != null)
            {
                string[] p = line.Split(" ".ToCharArray());
                switch (p[0])
                {

                    case "o":
                        name = p[1];
                        break;
                    case "g":
                        // buffer.PushGroup(p[1].Trim());
                        break;
                    case "v":
                        verticies.Add(new Vector3(cf(p[1]), cf(p[2]), cf(p[3])));
                        break;
                    case "vt":
                        uvs.Add(new Vector2(cf(p[1]), cf(p[2])));
                        break;
                    case "vn":
                        normals.Add(new Vector3(cf(p[1]), cf(p[2]), cf(p[3])));
                        break;
                    case "f":
                        List<int> intArray = new List<int>();
                        for (int i = 1; i < p.Length; i++) {
                            Vector3 temp = new Vector3();
                            string[] s = p[i].Split("/"[0]);
                            temp.x = System.Convert.ToInt32(s[0]);
                            if (s.Length > 1){
                                if (s[1] != "")
                                    temp.y = System.Convert.ToInt32(s[1]);
                            }
                            if (s.Length > 2) {
                                if (s[2] != "")
                                    temp.z = System.Convert.ToInt32(s[2]);
                            }
                            faces.Add(temp);
                            intArray.Add(int.Parse(s[0])-1);
                        }

                        for (int i = 1; i < (p.Length-2) ; ++i){
                            triangles.Add(intArray[0]);
                            triangles.Add(intArray[i]);
                            triangles.Add(intArray[i+1]);
                        }
                        break;
                    /*case "mtllib":
                        material = p[1].Trim();
                        break;*/
                    case "usemtl":
                        uml = p[1].Trim();
                        break;
                }
            }
        }

    }

    public static float cf(string input) {      //convert to float
        return float.Parse(input);
    }
    public static int ci(string input)
    {      //convert to float
        return int.Parse(input);
    }
}

