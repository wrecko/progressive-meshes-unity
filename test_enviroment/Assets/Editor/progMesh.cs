﻿using UnityEngine;
using System.Collections;
using System.Text;
using System.IO;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Globalization;
using UnityEditor;

public class progMesh{

    private List<Vertex> g_vertices = new List<Vertex>();
    private List<Triangle> g_triangles = new List<Triangle>();
    string name;
    //private Vertex[] g_vertices;
   // private Triangle[] g_triangles;

    public progMesh() {

    }

    public progMesh(List<Vector3> verticies, List<int> triangles, string name){
        this.name = name;

        for (int i = 0; i < verticies.Count; i++){
            g_vertices.Add(new Vertex(verticies[i], i));            
        }
        
        for (int i = 0; i < triangles.Count; i += 3){
            
            g_triangles.Add( new Triangle(  g_vertices[triangles[i]],
                                            g_vertices[triangles[i+1]],
                                            g_vertices[triangles[i+2]]));
           
        }

        ComputeAllEdgeCollapseCosts();
        process();
        //buildMesh();
        
    }

    void buildMesh()
    {
        List<Vector3> newVert = new List<Vector3>();
        List<int> newTri = new List<int>();
        for (int i = 0; i < g_vertices.Count; i++)
        {
            newVert.Add(g_vertices[i].pozicia);
        }
        //int tpp = g_vertices.IndexOf(g_triangles[0].vertex[0]);

        for (int i = 0; i < g_triangles.Count; i++)
        {
            
            newTri.Add(g_vertices.IndexOf(g_triangles[i].vertex[0]));
            newTri.Add(g_vertices.IndexOf(g_triangles[i].vertex[1]));
            newTri.Add(g_vertices.IndexOf(g_triangles[i].vertex[2]));
        }

        Mesh m = new Mesh();

        m.vertices = newVert.ToArray();
        m.triangles = newTri.ToArray();

        m.RecalculateNormals();
        m.RecalculateBounds();
        m.Optimize();

        AssetDatabase.CreateAsset(m,"Assets\\new.asset");
        AssetDatabase.SaveAssets();
        AssetDatabase.Refresh();

    }

    public void saveMesh(string perc) {

        using (StreamWriter w = File.AppendText(meshSimpTool.URL + "\\" + this.name +  "\\" + perc + ".bin"))
        {
            for (int i = 0; i < g_vertices.Count; i++)
            {
                w.Write(g_vertices[i].pozicia.x + "," + g_vertices[i].pozicia.y + "," + g_vertices[i].pozicia.z);
                if (i < g_vertices.Count - 1)
                    w.Write(':');
            }
            w.Write(";");
            for (int i = 0; i < g_triangles.Count; i++)
            {
                string tmp = g_vertices.IndexOf(g_triangles[i].vertex[0]).ToString() + ',' + g_vertices.IndexOf(g_triangles[i].vertex[1]).ToString() + ',' + g_vertices.IndexOf(g_triangles[i].vertex[2]).ToString();
                //if(perc == "99")Debug.Log(tmp);
                w.Write(tmp);
                if (i < g_triangles.Count - 1)
                    w.Write(',');
            }
            w.Close();
        }

    }

    Vertex MinimumCostEdge()
    {

        Vertex tmp = g_vertices[0];
        for (int i = 0; i < g_vertices.Count; i++)
        {
            if (g_vertices[i].objvzd < tmp.objvzd)
            {
                tmp = g_vertices[i];
            }
        }
        return tmp;

    }

    float ComputeEdgeCollapseCost(Vertex u, Vertex v)
    {
        int i;
        float edgeLength = Vector3.Magnitude(u.pozicia - v.pozicia);
        float curvature = 0;

        List<Triangle> sides = new List<Triangle>();

        for (i = 0; i < u.susedneTrian.Count; i++)
        {
            if (u.susedneTrian[i].maVertex(v))
            {
                sides.Add(u.susedneTrian[i]);
            }
        }

        for (i = 0; i < u.susedneTrian.Count; i++)
        {
            float mincurv = 1;
            for (int j = 0; j < sides.Count; j++)
            {
                float dotprod = Vector3.Dot(u.susedneTrian[i].normal, sides[j].normal);
                mincurv = Mathf.Min(mincurv, (1 - dotprod) / 2.0f);
            }
            curvature = Mathf.Max(curvature, mincurv);
        }

        return edgeLength * curvature;
    }

    void ComputeEdgeCostAtVertex(Vertex v)
    {
        if (v.susedneVrcholy.Count == 0)
        {
            v.collapse = null;
            v.objvzd = -0.01f;
            return;
        }
        v.collapse = null;
        v.objvzd = 100000;
        for (int i = 0; i < v.susedneVrcholy.Count; i++)
        {
            float vzdialenost;
            vzdialenost = ComputeEdgeCollapseCost(v, v.susedneVrcholy[i]);
            if (vzdialenost < v.objvzd)
            {
                v.collapse = v.susedneVrcholy[i];
                v.objvzd = vzdialenost;
            }
        }

    }

    void ComputeAllEdgeCollapseCosts()
    {

        for (int i = 0; i < g_vertices.Count; i++)
        {
            ComputeEdgeCostAtVertex(g_vertices[i]);
        }
    }

    void Colapse(Vertex u, Vertex v)
    {
        if((u != null && v != null) && (u._id == 1 || v._id == 1)) {
            Debug.Log("4444");
        }
        if (v == null)
        {
            g_vertices.Remove(u);
            u.pseudoDest();
            return;
        }
        List<Vertex> tmpSused = new List<Vertex>();
        for (int i = 0; i < u.susedneVrcholy.Count; i++)
        {
            tmpSused.Add(u.susedneVrcholy[i]);
        }

        for (int i = u.susedneTrian.Count - 1; i >= 0; i--)
        {
            if (u.susedneTrian[i].maVertex(v)){

                g_triangles.Remove(u.susedneTrian[i]);
                u.susedneTrian[i].pseudoDestructor();
          
            }
        }
        for (int i = u.susedneTrian.Count - 1; i >= 0; i--)
        {            
            u.susedneTrian[i].nahradVertex(u, v);
        }

        g_vertices.Remove(u);
        u.pseudoDest();

        for (int i = 0; i < tmpSused.Count; i++)
        {
            ComputeEdgeCostAtVertex(tmpSused[i]);
        }
    }

    void process()
    {

        int stovka = g_vertices.Count;
        int percenta = 99;

        while (g_vertices.Count > 0)
        {
            Vertex mn = MinimumCostEdge();

            Colapse(mn, mn.collapse);

            float vysledok = Mathf.Round((percenta * stovka) / 100);
            if (vysledok >= g_vertices.Count)
            {
                saveMesh(percenta.ToString());
                percenta--;
               // buildMesh(); break;

            }

        }
    }





    class Triangle
    {

        public Vertex[] vertex = new Vertex[3];
        public Vector3 normal;

        public Triangle(Vertex v1, Vertex v2, Vertex v3)
        {
            vertex[0] = v1;
            vertex[1] = v2;
            vertex[2] = v3;

            vypocitajNormal();

            for (int i = 0; i < 3; i++){

                vertex[i].susedneTrian.Add(this);
                for (int j = 0; j < 3; j++)
                    if (i != j){
                        vertex[i].pridajUniqueVrchol(vertex[j]);
                    }
            }
        }

        public void pseudoDestructor () {
            
            for (int i = 0; i < 3; i++) {
                if (vertex[i] != null)
                    vertex[i].susedneTrian.Remove(this);
            }
            for (int i = 0; i < 3; i++){

                int i2 = (i + 1) % 3;
                if (vertex[i] == null || vertex[i2] == null) continue;
                vertex[i].vymazAkNieSused(vertex[i2]);
                vertex[i2].vymazAkNieSused(vertex[i]);
            }
        }

        private void vypocitajNormal()
        {
            Vector3 side1 = vertex[1].pozicia - vertex[0].pozicia;
            Vector3 side2 = vertex[2].pozicia - vertex[0].pozicia;
            normal = Vector3.Cross(side1, side2).normalized;
        }

        public bool maVertex(Vertex v)
        {
            return (v == vertex[0] || v == vertex[1] || v == vertex[2]);
        }

        public void nahradVertex(Vertex vStar, Vertex vNov)
        {
            Debug.Assert(vStar!=null && vNov !=null);
            Debug.Assert(vStar == vertex[0] || vStar == vertex[1] || vStar == vertex[2]);
            Debug.Assert(vNov != vertex[0] && vNov != vertex[1] && vNov != vertex[2]);



            if (vStar == vertex[0])
            {
                vertex[0] = vNov;
            }
            else if (vStar == vertex[1])
            {
                vertex[1] = vNov;
            }
            else
            {
                if (vStar != vertex[2]) return;
                vertex[2] = vNov;
            }

            vStar.susedneTrian.Remove(this);
            Debug.Assert(!vNov.susedneTrian.Contains(this));
            vNov.susedneTrian.Add(this);

            for (int i = 0; i < 3; i++)
            {
                vStar.vymazAkNieSused(vertex[i]);
                vertex[i].vymazAkNieSused(vStar);
            }

            for (int i = 0; i < 3; i++)
            {
                Debug.Assert(vertex[i].susedneTrian.Contains(this));
                for (int j = 0; j < 3; j++)
                    if (i != j)
                    {
                        vertex[i].pridajUniqueVrchol(vertex[j]);
                    }
            }

            vypocitajNormal();
        }

    }

    class Vertex
    {

        public Vector3 pozicia;
        public int _id;
        public IList<Vertex> susedneVrcholy = new List<Vertex>();       //neighbor
        public IList<Triangle> susedneTrian = new List<Triangle>();     //face
        public Vertex collapse;   //vertex pre collapse
        public float objvzd;     //ulozena vzdialenost pre collapse

        public Vertex(Vector3 v, int _id)
        {
            this.pozicia = v;
            this._id = _id;
        }
        public void pseudoDest (){
            while (susedneVrcholy.Count > 0){

                susedneVrcholy[0].susedneVrcholy.Remove(this);
                susedneVrcholy.Remove(susedneVrcholy[0]);
            }
        }

        public void pridajUniqueVrchol(Vertex v)
        {
            if (susedneVrcholy.Contains(v))
                return;
            else
                susedneVrcholy.Add(v);
        }

        public void vymazAkNieSused(Vertex n)
        {
            if (!susedneVrcholy.Contains(n)) return;
            for (int i = 0; i < susedneTrian.Count; i++)
            {
                if (susedneTrian[i].maVertex(n)) return;
            }

            susedneVrcholy.Remove(n);
        }
    }

}
