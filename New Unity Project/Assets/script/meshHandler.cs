﻿using UnityEngine;
using System.Collections;

public class meshHandler : MonoBehaviour {

    public loadMesh LM;
    public progressiveMeshes pm;
    public float sliderVal;

    private float actualSliderVal;
    private float lastVal;
    private GameObject renderedMesh;
    private bool wired = false;

    private UnityEngine.UI.Slider slider;

    // Use this for initialization
    void Start () {
        LM = new loadMesh();
        pm = new progressiveMeshes();

        slider = GameObject.Find("Slider").GetComponent<UnityEngine.UI.Slider>();
        slider.maxValue = 20;

        actualSliderVal = buttonOnClick.baseMesh;
        lastVal = buttonOnClick.baseMesh;
        renderedMesh = GameObject.Find("renderedMesh");
    }
	
	// Update is called once per frame
	void Update () {        
        sliderVal = slider.value;
    }

    public void onSliderChange() {

        if(LM.m != null)
            if (sliderVal < actualSliderVal){
                /*pm.process(sliderVal);
                renderedMesh.GetComponent<MeshFilter>().mesh = pm.buildMesh();*/
                startLoadMeshesDown((int)sliderVal);
            }else {
                //pm.vertexSplit(sliderVal);
                //renderedMesh.GetComponent<MeshFilter>().mesh = pm.vertexSplit(sliderVal, 5);    
                startLoadMeshes((int)sliderVal);
            }
        
        actualSliderVal = sliderVal;
    }    

    public void wiredModel() {
        if (!wired) {
            GL.wireframe = false;
        }
    }

    public void reset() {
        renderedMesh.GetComponent<MeshFilter>().mesh = new Mesh();
        pm.destroy();
<<<<<<< HEAD
        sliderVal = 100;
       /* GameObject.Find("Slider").GetComponent<UnityEngine.UI.Slider>().interactable = false;
        GameObject.Find("Slider").GetComponent<UnityEngine.UI.Slider>().value = 100;*/
=======
        Debug.Log("Reset");
    }

    /**
    *   Loading partials from server - UP
    */

    public void startLoadMeshes(int percentage) {
        Debug.Log("Started UP");
        for (int i = (int)lastVal; i < percentage; i++)
        {
            StartCoroutine(wwwRequest(percentage * 5));            
        }
        lastVal = percentage;
>>>>>>> new/feature
    }

    /**
    *   Loading partials from server - Down
    */

    public void startLoadMeshesDown(int percentage)
    {
        Debug.Log("Started Down");
        for (int i = (int)lastVal; i > percentage; i--)
        {
            StartCoroutine(wwwRequest(percentage * 5));
        }
        lastVal = percentage;
    }

    /**
    *   Request on server
    */

    private IEnumerator wwwRequest(int percentage)
    {
        string path = buttonOnClick.URL + LM.name + "/" + percentage + ".bin";

        WWW request = new WWW(path);
        yield return request;

        if (request != null)
        {
            loadMesh temp = new loadMesh();
            temp.processMesh(request.text, LM.name);
            //System.Threading.Thread.Sleep(500);
            renderedMesh.GetComponent<MeshFilter>().mesh = temp.m;
            
        }       
                
    }

}
