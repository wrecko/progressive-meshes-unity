﻿using UnityEngine;
using System.Collections;

public class meshRotate : MonoBehaviour {

    private Transform cameraa;
    private float sensitivityX = 80, sensitivityY = 80;
    private bool drag;


    // Use this for initialization
    void Start () {
        cameraa = Camera.main.transform;
    }
	
	// Update is called once per frame
	void Update () {
        if (drag){
            float rotX = Input.GetAxis("Mouse X") * sensitivityX;
            float rotY = Input.GetAxis("Mouse Y") * sensitivityY;

            transform.Rotate(cameraa.up, -Mathf.Deg2Rad * rotX);
            transform.Rotate(cameraa.right, Mathf.Deg2Rad * rotY);
        }

    }
    void OnMouseDown() {

        drag = true;
        //Debug.Log("haii ");
    }

    void OnMouseUp() {
        drag = false;
    }
}
