﻿using UnityEngine;
using System.Collections.Generic;


public class progMeshDS
{
    public List<Vertex> g_vertices;
    public List<Triangle> g_triangles;

    public progMeshDS prev;
    public int fullSize;

    public progMeshDS() {
        g_vertices = new List<Vertex>();
        g_triangles = new List<Triangle>();
        prev = null;
    }

    public void copy(List<Vertex> vert, List<Triangle> tria, progMeshDS inst) {

        vert.ForEach((item) => {
            g_vertices.Add(item);
        });

        tria.ForEach((item) => {
            g_triangles.Add(item);
        });

        prev = inst;

        fullSize = g_vertices.Count;
        
    }

}

public class Triangle
{

    public Vertex[] vertex = new Vertex[3];
    public Vector3 normal;

    public Triangle(Vertex v1, Vertex v2, Vertex v3)
    {
        vertex[0] = v1;
        vertex[1] = v2;
        vertex[2] = v3;

        vypocitajNormal();

        for (int i = 0; i < 3; i++)
        {

            vertex[i].susedneTrian.Add(this);
            for (int j = 0; j < 3; j++)
                if (i != j)
                {
                    vertex[i].pridajUniqueVrchol(vertex[j]);
                }
        }
    }

    public void pseudoDestructor()
    {

        for (int i = 0; i < 3; i++)
        {
            if (vertex[i] != null)
                vertex[i].susedneTrian.Remove(this);
        }
        for (int i = 0; i < 3; i++)
        {

            int i2 = (i + 1) % 3;
            if (vertex[i] == null || vertex[i2] == null) continue;
            vertex[i].vymazAkNieSused(vertex[i2]);
            vertex[i2].vymazAkNieSused(vertex[i]);
        }
    }

    private void vypocitajNormal()
    {
        Vector3 side1 = vertex[1].position - vertex[0].position;
        Vector3 side2 = vertex[2].position - vertex[0].position;
        normal = Vector3.Cross(side1, side2).normalized;
    }

    public bool maVertex(Vertex v)
    {
        return (v == vertex[0] || v == vertex[1] || v == vertex[2]);
    }

    public void nahradVertex(Vertex vStar, Vertex vNov)
    {
        Debug.Assert(vStar != null && vNov != null);
        Debug.Assert(vStar == vertex[0] || vStar == vertex[1] || vStar == vertex[2]);
        Debug.Assert(vNov != vertex[0] && vNov != vertex[1] && vNov != vertex[2]);



        if (vStar == vertex[0])
        {
            vertex[0] = vNov;
        }
        else if (vStar == vertex[1])
        {
            vertex[1] = vNov;
        }
        else
        {
            if (vStar != vertex[2]) return;
            vertex[2] = vNov;
        }

        vStar.susedneTrian.Remove(this);
        Debug.Assert(!vNov.susedneTrian.Contains(this));
        vNov.susedneTrian.Add(this);

        for (int i = 0; i < 3; i++)
        {
            vStar.vymazAkNieSused(vertex[i]);
            vertex[i].vymazAkNieSused(vStar);
        }

        for (int i = 0; i < 3; i++)
        {
            Debug.Assert(vertex[i].susedneTrian.Contains(this));
            for (int j = 0; j < 3; j++)
                if (i != j)
                {
                    vertex[i].pridajUniqueVrchol(vertex[j]);
                }
        }

        vypocitajNormal();
    }

}

public class Vertex
{

    public Vector3 position;
    public int _id;
    public IList<Vertex> susedneVrcholy = new List<Vertex>();       //neighbor
    public IList<Triangle> susedneTrian = new List<Triangle>();     //face
    public Vertex collapse;   //vertex pre collapse
    public float objvzd;     //ulozena vzdialenost pre collapse

    public Vertex(Vector3 v, int _id)
    {
        this.position = v;
        this._id = _id;
    }

    public void pseudoDest()
    {
        while (susedneVrcholy.Count > 0)
        {

            susedneVrcholy[0].susedneVrcholy.Remove(this);
            susedneVrcholy.Remove(susedneVrcholy[0]);
        }
    }

    public void pridajUniqueVrchol(Vertex v)
    {
        if (susedneVrcholy.Contains(v))
            return;
        else
            susedneVrcholy.Add(v);
    }

    public void vymazAkNieSused(Vertex n)
    {
        if (!susedneVrcholy.Contains(n)) return;
        for (int i = 0; i < susedneTrian.Count; i++)
        {
            if (susedneTrian[i].maVertex(n)) return;
        }

        susedneVrcholy.Remove(n);
    }
}

