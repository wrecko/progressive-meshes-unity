﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System;

public class buttonOnClick : MonoBehaviour {


    public static string URL = "http://www.wrecko.esy.es/obj_data/";
    public static int baseMesh = 60/5;

    private loadMesh LM;
    private progressiveMeshes pm;
    private Dropdown DD;
    private string meshData;
    private GameObject form;

    void Start() {
        DD = GameObject.Find("Dropdown").GetComponent<Dropdown>();

        GameObject tmp = GameObject.Find("Main Camera");
        form = GameObject.Find("upload file");
        LM = tmp.GetComponent<meshHandler>().LM;
        pm = tmp.GetComponent<meshHandler>().pm;

        form.active = false;
    }

    public void LoadMeshWWW() {

        StartCoroutine(wwwRequest(URL + DD.options[DD.value].text + "/"+ (baseMesh*5) +".bin"));  
    }
    private void drawmesh(Mesh mesh){

        GameObject tmp = GameObject.Find("renderedMesh");
        tmp.GetComponent<MeshRenderer>().material.shader = Shader.Find("Standard");
        tmp.GetComponent<MeshFilter>().mesh = mesh;
    }

    public void showForm() {
        form.active = !form.active;
    }

    public void sendRequest() {
        Debug.Log("Uploading");
        string path = GameObject.Find("uploadField").GetComponent<Text>().text;
        sendPostRequest spr = new sendPostRequest(path);
        StartCoroutine(spr.sendRequest());
        form.active = false;
        
    }

    private IEnumerator wwwRequest2(string path)
    {

        WWW request = new WWW(path);
        yield return request;
        if (request != null)
        {
            LM.processMesh(request.data, DD.options[DD.value].text);
            pm.buildStructure(LM.m.vertices, LM.m.triangles);
            int complexity;
            bool check = Int32.TryParse(GameObject.Find("textComplexity").GetComponent<Text>().text, out complexity);
            if (check && complexity < 100){
                pm.process(complexity);
                GameObject.Find("Slider").GetComponent<Slider>().interactable = true;
                GameObject.Find("Slider").GetComponent<Slider>().value = complexity;
                drawmesh(pm.buildMesh());
            }else
                drawmesh(LM.m);
        }
        
    }
    private IEnumerator wwwRequest(string path)
    {
        WWW request = new WWW(path);
        yield return request;
        if (request != null)
        {
            GameObject.Find("Slider").GetComponent<Slider>().value = baseMesh;

            LM.processMesh(request.text, DD.options[DD.value].text);            
            drawmesh(LM.m);
        }
    }

}
