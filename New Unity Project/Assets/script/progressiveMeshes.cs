﻿using UnityEngine;
using System.Collections.Generic;
using System;

public class progressiveMeshes
{

    private List<Vertex> g_vertices;
    private List<Triangle> g_triangles;
            
    Dictionary<int, Mesh> ds = new Dictionary<int, Mesh>();
    progMeshDS orig = new progMeshDS();
    progMeshDS actual = new progMeshDS();

    private int fullSize;

    public progressiveMeshes() {
        g_vertices = actual.g_vertices;
        g_triangles = actual.g_triangles;
    }

    public void destroy() {
        g_vertices.Clear();
        g_triangles.Clear();
        ds.Clear();
        actual = null;
    }

    public void buildStructure(Vector3[] verticies, int[] triangles)
    {
        
        for (int i = 0; i < verticies.Length; i++)
        {
            g_vertices.Add(new Vertex(verticies[i], i));
        }

        for (int i = 0; i < triangles.Length; i += 3)
        {

            g_triangles.Add(new Triangle(g_vertices[triangles[i]],
                                            g_vertices[triangles[i + 1]],
                                            g_vertices[triangles[i + 2]]));

        }
        fullSize = g_vertices.Count;
        orig.copy(g_vertices, g_triangles, actual);
        ds.Add(fullSize, buildMesh());

        ComputeAllEdgeCollapseCosts();

    }

    public Mesh buildMesh()
    {
        List<Vector3> newVert = new List<Vector3>();
        List<int> newTri = new List<int>();
        for (int i = 0; i < g_vertices.Count; i++)
        {
            newVert.Add(g_vertices[i].position);
        }

        for (int i = 0; i < g_triangles.Count; i++)
        {

            newTri.Add(g_vertices.IndexOf(g_triangles[i].vertex[0]));
            newTri.Add(g_vertices.IndexOf(g_triangles[i].vertex[1]));
            newTri.Add(g_vertices.IndexOf(g_triangles[i].vertex[2]));
        }

        //Debug.Log(percentage);
        //progMeshes[percentage] = new progMesh(newVert.ToArray(), newTri.ToArray());
        Mesh m = new Mesh();

        m.vertices = newVert.ToArray();
        m.triangles = newTri.ToArray();

        m.RecalculateNormals();
        m.RecalculateBounds();
        m.Optimize();

        return m;


    }

    Vertex MinimumCostEdge()
    {

        Vertex tmp = g_vertices[0];
        for (int i = 0; i < g_vertices.Count; i++)
        {
            if (g_vertices[i].objvzd < tmp.objvzd)
            {
                tmp = g_vertices[i];
            }
        }
        return tmp;

    }

    float ComputeEdgeCollapseCost(Vertex u, Vertex v)
    {
        int i;
        float edgeLength = Vector3.Magnitude(u.position - v.position);
        float curvature = 0;

        List<Triangle> sides = new List<Triangle>();

        for (i = 0; i < u.susedneTrian.Count; i++)
        {
            if (u.susedneTrian[i].maVertex(v))
            {
                sides.Add(u.susedneTrian[i]);
            }
        }

        for (i = 0; i < u.susedneTrian.Count; i++)
        {
            float mincurv = 1;
            for (int j = 0; j < sides.Count; j++)
            {
                float dotprod = Vector3.Dot(u.susedneTrian[i].normal, sides[j].normal);
                mincurv = Mathf.Min(mincurv, (1 - dotprod) / 2.0f);
            }
            curvature = Mathf.Max(curvature, mincurv);
        }

        return edgeLength * curvature;
    }

    void ComputeEdgeCostAtVertex(Vertex v)
    {
        if (v.susedneVrcholy.Count == 0)
        {
            v.collapse = null;
            v.objvzd = -0.01f;
            return;
        }
        v.collapse = null;
        v.objvzd = 100000;
        for (int i = 0; i < v.susedneVrcholy.Count; i++)
        {
            float vzdialenost;
            vzdialenost = ComputeEdgeCollapseCost(v, v.susedneVrcholy[i]);
            if (vzdialenost < v.objvzd)
            {
                v.collapse = v.susedneVrcholy[i];
                v.objvzd = vzdialenost;
            }
        }

    }

    void ComputeAllEdgeCollapseCosts()
    {

        for (int i = 0; i < g_vertices.Count; i++)
        {
            ComputeEdgeCostAtVertex(g_vertices[i]);
        }
    }

    void Colapse(Vertex u, Vertex v)
    {
        if ((u != null && v != null) && (u._id == 1 || v._id == 1))
        {
            Debug.Log("Fail");
        }
        if (v == null)
        {
            g_vertices.Remove(u);
            u.pseudoDest();
            return;
        }
        List<Vertex> tmpSused = new List<Vertex>();
        for (int i = 0; i < u.susedneVrcholy.Count; i++)
        {
            tmpSused.Add(u.susedneVrcholy[i]);
        }

        for (int i = u.susedneTrian.Count - 1; i >= 0; i--)
        {
            if (u.susedneTrian[i].maVertex(v))
            {

                g_triangles.Remove(u.susedneTrian[i]);
                u.susedneTrian[i].pseudoDestructor();

            }
        }
        for (int i = u.susedneTrian.Count - 1; i >= 0; i--)
        {
            u.susedneTrian[i].nahradVertex(u, v);
        }

        g_vertices.Remove(u);
        u.pseudoDest();

        for (int i = 0; i < tmpSused.Count; i++)
        {
            ComputeEdgeCostAtVertex(tmpSused[i]);
        }
    }

    public void process(float complexity)
    {
        float result = Mathf.Round((fullSize / 100) * complexity);
       // Debug.Log(result);
        while (g_vertices.Count > result)
        {
            Vertex mn = MinimumCostEdge();

            Colapse(mn, mn.collapse);

            Mesh m = buildMesh();
            ds.Add(g_vertices.Count, m);


        }
    }

    public void vertexSplit(float complexity){
        if (complexity == 100.0)
            return;

        float result = Mathf.Round((fullSize / 100) * complexity);
        int count = g_vertices.Count;
        progMeshDS temp = actual;

        while (count < result) {
            temp = temp.prev;
            count = temp.fullSize;
        }

        actual = temp;
        g_vertices = actual.g_vertices;
        g_triangles = actual.g_triangles;


    }

    public Mesh vertexSplit(float complexity, int i)
    {
        if (complexity == 100.0)
            return null;

        float result = Mathf.Round((fullSize / 100) * complexity);

       /* actual = orig;
        g_vertices = actual.g_vertices;
        g_triangles = actual.g_triangles;*/

        return ds[(int)result];


    }


}
