﻿using UnityEngine;
using System.Collections;

public class meshScale : MonoBehaviour {

    private Transform camera;

    // Use this for initialization
    void Start () {
        camera = Camera.main.transform;
    }
	
	// Update is called once per frame
	void Update () {
        float d = Input.GetAxis("Mouse ScrollWheel");

        if (d > 0f){
            camera.Translate(new Vector3(0.0f, 0.0f , d));
        }
        else if (d < 0f){
            camera.Translate(new Vector3(0.0f, 0.0f, d));
        }
    }
}
