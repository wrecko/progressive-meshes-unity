﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class loadMeshList : MonoBehaviour{

    public string URL = "http://wrecko.esy.es/obj_data/";
    private Dropdown DD;

    // Use this for initialization
    void Start () {
        DD = GetComponent<Dropdown>();        
        StartCoroutine(loadListFromServer());

    }
    /*
    * Load Mesh list from server
    */
    private IEnumerator loadListFromServer() {
        WWW listURL = new WWW(URL + "list.txt");
        yield return listURL;

        GameObject.Find("textComplexity").GetComponent<Text>().text = "100";
        if (listURL != null){
            string[] meshList = listURL.text.Split(';');

            foreach(string s in meshList) {
                if (s == "") continue;
                DD.options.Add(new Dropdown.OptionData(s));
            }
        }        
    }
        
}
