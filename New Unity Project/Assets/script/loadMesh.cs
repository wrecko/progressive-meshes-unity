﻿using UnityEngine;
using System.Collections;

public class loadMesh{

    public Mesh m;
    public string name;

    private Vector3[] verticies;
    private int[] triangles;

    public Mesh returnMesh() { return m; }

    public loadMesh(string data)
    {
        if(data != null) {
            parseInputFile(data);
            buildMesh();
        }  
    }
    public loadMesh(){}

    public void processMesh(string data, string name)
    {
        if (data != null)
        {
            this.name = name;
            parseInputFile(data);
            buildMesh();
        }
    }

    private void buildMesh()
    {
        m = new Mesh();
        m.vertices = verticies;
        m.triangles = triangles;

        m.RecalculateNormals();
        m.RecalculateBounds();
        m.Optimize();

    }
    
    private void buildMesh(Vector3[] verticies, int[] triangles)
    {
        m = new Mesh();
        m.vertices = verticies;
        m.triangles = triangles;

        m.RecalculateNormals();
        m.RecalculateBounds();
        m.Optimize();

    }

    private void parseInputFile(string data)
    {
        string tmp = data;
        string[] param = data.Split(';');
        string[] verticiesRaw = param[0].Split(':');

        verticies = new Vector3[verticiesRaw.Length];

        for (int i = 0; i < verticiesRaw.Length; i++)
        {
            string[] vals = verticiesRaw[i].Split(',');
            verticies[i] = new Vector3(loadMesh.cf(vals[0]), loadMesh.cf(vals[1]), loadMesh.cf(vals[2]));
        }
        string[] trianRaw = param[1].Split(',');
        triangles = new int[trianRaw.Length];
        for (int i = 0; i < trianRaw.Length; i++)
        {
            triangles[i] = loadMesh.ci(trianRaw[i]);
        }
    }

    public static float cf(string input)
    {      //convert to float
        return float.Parse(input);
    }
    public static int ci(string input)
    {      //convert to int
        return int.Parse(input);
    }

}
	

