﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Text;

public class sendPostRequest {
    string URL = "http://www.wrecko.esy.es/" + "saveMesh.php";
    string material, uml, name = "3Dmodel";
    List<Vector3> verticies = new List<Vector3>();
    List<Vector2> uvs = new List<Vector2>();
    List<Vector3> normals = new List<Vector3>();
    List<Vector3> faces = new List<Vector3>();
    List<int> triangles = new List<int>();

    public sendPostRequest(string path) {
        if (path == "" || path == null)
        {
            Debug.Log("Mesh could be loaded");
            return;
        }

        StreamReader sr = new StreamReader(path, Encoding.Default);
        process(sr);
        sr.Close();

        //sendRequest();
    }

    public IEnumerator sendRequest() {

        var form = new WWWForm();
        form.AddField("vertices", returnVerticesString());
        form.AddField("triangles", returnTrianglesString());

        var www = new WWW(URL, form);
        yield return www;
        
        if (www.error == null){
            Debug.Log("Response: " + www.text);
        }
        else{            
            Debug.Log("WWW Error: " + www.error);
        }
    }

    private string returnVerticesString() {
        string result = "";
        for (int i = 0; i < verticies.Count; i++)
        {
            result += verticies[i].x + "," + verticies[i].y + "," + verticies[i].z;
            /*if (i < verticies.Count - 1)
                result += ':';*/
        }
        return result;
    }

    private string returnTrianglesString() {
        string result = "";
        for (int i = 0; i < triangles.Count; i++)
        {
            result += triangles[i];
            if (i < triangles.Count - 1)
                result += ',';
        }
        return result;
    }


    private void process(StreamReader input)
    {

        string line;
        while (!input.EndOfStream)
        {

            line = input.ReadLine();

            if (line == "" || line[0] == '#') continue;

            if (line != null)
            {
                string[] p = line.Split(" ".ToCharArray());
                p = checkForBlanks(p);
                switch (p[0])
                {

                    case "o":
                        name = p[1];
                        break;
                    case "g":
                        // buffer.PushGroup(p[1].Trim());
                        break;
                    case "v":
                        if (p[1] == "")
                            verticies.Add(new Vector3(cf(p[2]), cf(p[3]), cf(p[4])));
                        else
                            verticies.Add(new Vector3(cf(p[1]), cf(p[2]), cf(p[3])));
                        break;
                    case "vt":
                        uvs.Add(new Vector2(cf(p[1]), cf(p[2])));
                        break;
                    case "vn":
                        normals.Add(new Vector3(cf(p[1]), cf(p[2]), cf(p[3])));
                        break;
                    case "f":
                        List<int> intArray = new List<int>();
                        for (int i = 1; i < p.Length; i++)
                        {
                            if (p[i] == "")
                                continue;

                            Vector3 temp = new Vector3();
                            string[] s = p[i].Split("/"[0]);
                            temp.x = System.Convert.ToInt32(s[0]);
                            if (s.Length > 1)
                            {
                                if (s[1] != "")
                                    temp.y = System.Convert.ToInt32(s[1]);
                            }
                            if (s.Length > 2)
                            {
                                if (s[2] != "")
                                    temp.z = System.Convert.ToInt32(s[2]);
                            }
                            faces.Add(temp);
                            intArray.Add(int.Parse(s[0]) - 1);
                        }

                        for (int i = 1; i < (p.Length - 2); ++i)
                        {
                            triangles.Add(intArray[0]);
                            triangles.Add(intArray[i]);
                            triangles.Add(intArray[i + 1]);
                        }
                        break;
                    /*case "mtllib":
                        material = p[1].Trim();
                        break;*/
                    case "usemtl":
                        uml = p[1].Trim();
                        break;
                }
            }
        }

    }

    private string[] checkForBlanks(string[] input)
    {
        List<string> result = new List<string>();
        foreach (string s in input)
        {
            if (s != "") result.Add(s);
        }

        return result.ToArray();
    }

    public static float cf(string input)
    {      //convert to float
        return float.Parse(input);
    }
    public static int ci(string input)
    {      //convert to float
        return int.Parse(input);
    }
}
	

