<?php
namespace App\Libs;

use Intervention\Image\Facades\Image;

class Photos
{
    private $_trainerPath = 'photos/t/';
    private $_clientPath = 'photos/c/';
    private $_iconPath = 'photos/icons/';

    public function saveNewImage($request, $class = 0, $person = null){
        switch($class){
            case 0:
                foreach($request->file('images') as $i => $image){
                    $profile = ($request->get('profile') == $i)? 1:0;
                    $img = $this->processImage($image, $this->_trainerPath);
                    $this->saveImageTrainer($img, $profile, $person);
                }
                break;
            case 1:
                $img = $this->processImage($request, $this->_clientPath);
                $this->saveImageClient($img, $person);
                break;
            case 2:
                $img = $this->processImage($request, $this->_iconPath);
                return $img['path'];
        }
    }

    private function processImage($image, $folder){
        $result = [];

        $img = Image::make($image);
        if(!file_exists($folder)){
            \Storage::makeDirectory( $folder, 0777, true );
        }
        $path = $folder . str_random(20) . '.' . $image->getClientOriginalExtension();

        $result['path'] = $path;
        $result['img'] = $img;

        $img->save(storage_path("app" . DIRECTORY_SEPARATOR . $path));

        return $result;
    }

    private function saveImageTrainer($img, $profile, $trainer){
        $trainer->photos()->create([
            'photo'     => $img['path'],
            'status'    => ($profile)? 1:0,
            'width'     => $img['img']->width(),
            'height'    => $img['img']->height(),
            't_user_id' => $trainer->id
        ]);
    }

    private function saveImageClient($img, $client){

    }

}